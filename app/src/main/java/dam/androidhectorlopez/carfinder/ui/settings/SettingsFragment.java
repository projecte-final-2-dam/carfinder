package dam.androidhectorlopez.carfinder.ui.settings;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import dam.androidhectorlopez.carfinder.MainActivity;
import dam.androidhectorlopez.carfinder.R;
import dam.androidhectorlopez.carfinder.db.DBHelper;
import dam.androidhectorlopez.carfinder.db.DBManager_cotxes;

import static java.lang.Thread.sleep;

public class SettingsFragment extends Fragment {

    private Button btFabrica;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        btFabrica = root.findViewById(R.id.btFabrica);


//        btFabrica.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showAlert();
//            }
//        });
        btFabrica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert();
            }
        });

        return root;

    }

    private void showAlert() {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(R.string.btFabrica).setMessage(R.string.dialog_Reset).setPositiveButton(R.string.btAccept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                showAlert2();
            }
        }).setNegativeButton(R.string.btCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                try {
                    finalize();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });

        dialog.show();
    }


    private void showAlert2() {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(R.string.btFabrica).setMessage(R.string.dialog_Sure).setPositiveButton(R.string.btAccept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                getContext().deleteDatabase("cars.DB");
                DBHelper dbHelper = DBHelper.getInstance(getContext());
                dbHelper.onDelete(dbHelper.getWritableDatabase());
                dbHelper.onCreate(dbHelper.getWritableDatabase());
                showAlert3();

            }
        }).setNegativeButton(R.string.btCancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                try {
                    finalize();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });

        dialog.show();
    }


    private void showAlert3() {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(R.string.btFabrica).setMessage(R.string.dialog_Reseted).setPositiveButton(R.string.btAccept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                try {
                    finalize();

                    getActivity().finish();
                    Intent i = getContext().getPackageManager()
                            .getLaunchIntentForPackage(getContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    sleep(1000);

                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });

        dialog.show();
    }



}