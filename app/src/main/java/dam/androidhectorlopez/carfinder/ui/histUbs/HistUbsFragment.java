package dam.androidhectorlopez.carfinder.ui.histUbs;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import dam.androidhectorlopez.carfinder.R;
import dam.androidhectorlopez.carfinder.db.DBManager_ubicacions;
import dam.androidhectorlopez.carfinder.models.Ubicacio;

public class HistUbsFragment extends Fragment {


    private GoogleMap googleMap;
    private LocationManager locationManager;
    MapView mMapView;
    private DBManager_ubicacions dbManager_ubicacions;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_hist_ubicacions, container, false);
        mMapView = (MapView) root.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        dbManager_ubicacions = new DBManager_ubicacions(getContext());
        mMapView.onResume(); // needed to get the map to display immediately
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                googleMap.setMyLocationEnabled(true);

                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    ArrayList<Ubicacio> ubicacions = dbManager_ubicacions.getUbs();
//                    ArrayList<Ubicacio> ubicacions = dbManager_ubicacions.getLastsUbs();
                    Log.i("MAPA", "Tamany " + ubicacions.size());
                    for (Ubicacio ubicacio : ubicacions) {
                        LatLng latLng = new LatLng(ubicacio.getLat(), ubicacio.getLon());
                        Log.i("MAPA", ubicacio.getNickCar() + " " + ubicacio.getId());
                        googleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.iconococheblanco)).position(latLng).title(ubicacio.getIdCar() + " " + ubicacio.getNickCar()));
                        float zoom = 12;
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
                    }
                }
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}