package dam.androidhectorlopez.carfinder.ui.afegirVehicles;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import dam.androidhectorlopez.carfinder.bluetooth.AddBluetoothDevice;
import dam.androidhectorlopez.carfinder.R;
import dam.androidhectorlopez.carfinder.db.DBManager_cotxes;


public class AfegirVehiclesFragment extends Fragment {

    private EditText etNick;
    private EditText etBrand;
    private EditText etModel;
    private EditText etPlate;
    private CheckBox cbBluetooth;
    private Button btAdd;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View root = inflater.inflate(R.layout.fragment_afegir_vehicles, container, false);

        etNick = root.findViewById(R.id.etNick);
        etBrand = root.findViewById(R.id.etBrand);
        etModel = root.findViewById(R.id.etModel);
        etPlate = root.findViewById(R.id.etMatricula);
        cbBluetooth = root.findViewById(R.id.cbBluetooth);
        btAdd = root.findViewById(R.id.btAdd);

        btAdd.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                if (etNick.getText().toString().isEmpty() || etBrand.getText().toString().isEmpty() || etModel.getText().toString().isEmpty() || etPlate.getText().toString().isEmpty()) {
                    Toast.makeText(root.getContext(), R.string.emptyData, Toast.LENGTH_LONG).show();
                } else {
                    InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputMethodManager.hideSoftInputFromWindow(etPlate.getWindowToken(), 0);
                    DBManager_cotxes dbManager = new DBManager_cotxes(getContext());
                    dbManager.insert(etNick.getText().toString(),
                            etBrand.getText().toString(),
                            etModel.getText().toString(),
                            etPlate.getText().toString(),
                            cbBluetooth.isChecked());
                    dbManager.close();
                    if (cbBluetooth.isChecked()) {
                        AddBluetoothDevice add = new AddBluetoothDevice(getContext());
                        add.show();
                    } else {
                        showAlert();
                    }


                    etNick.setText("");
                    etBrand.setText("");
                    etModel.setText("");
                    etPlate.setText("");
                    cbBluetooth.setChecked(false);

                }
            }

        });

        return root;
    }

    private void showAlert() {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(R.string.menu_afegir).setMessage(R.string.dialog_car).setPositiveButton(R.string.btAccept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                try {
                    finalize();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });

        dialog.show();
    }


}

