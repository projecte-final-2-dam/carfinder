package dam.androidhectorlopez.carfinder.ui.afegirUbicacioManual;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import dam.androidhectorlopez.carfinder.R;
import dam.androidhectorlopez.carfinder.db.DBManager_cotxes;
import dam.androidhectorlopez.carfinder.db.DBManager_ubicacions;

public class LastLocationFragment extends Fragment {
    private LocationManager locationManager;

    private Spinner spCars;
    private Button btAdd;
    View root;
    DBManager_cotxes dbManager_cotxes;
    DBManager_ubicacions dbManager_ubicacions;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_last_location, container, false);

        dbManager_cotxes = new DBManager_cotxes(getContext());
        dbManager_ubicacions = new DBManager_ubicacions(getContext());

        setUI();
        return root;
    }

    private void setUI() {

        spCars = root.findViewById(R.id.spCars);
        btAdd = root.findViewById(R.id.btAddUbi);
        ArrayList<String> cotxes = dbManager_cotxes.getUbCar();

        spCars.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, cotxes));

        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    String id = spCars.getSelectedItem().toString();
                    String[] parts = id.split("\t");
                    int idCar = Integer.parseInt(parts[0]);

                    locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    dbManager_ubicacions.insert(idCar, location.getLongitude(), location.getLatitude());


                    showAlert();
                }

            }
        });


    }


    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(R.string.ubication).setMessage(R.string.dialog_ubic).setPositiveButton(R.string.btAccept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                try {
                    finalize();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });

        dialog.show();
    }
}