package dam.androidhectorlopez.carfinder.ui.about;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import dam.androidhectorlopez.carfinder.R;

public class AboutFragment extends Fragment {
    private Button bt_Mail_to;
    private Button bt_Twitter;
    private Intent intent;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_about, container, false);

        bt_Mail_to = root.findViewById(R.id.btMail_to);
        bt_Twitter = root.findViewById(R.id.btTwitter);

        /**
         * Aci al clickar fa un intent per a enviar un correu electronic al desarrollador
         */
        bt_Mail_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] addresses = {"lotehector@gmail.com"};
                intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, addresses);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Carfinder Help");
                if (intent.resolveActivity(getContext().getApplicationContext().getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });


        /**
         * Aci obrim el perfil de twitter del desarrollador
         */
        bt_Twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PackageInfo info = null;
                try {
                    info = getContext().getApplicationContext().getPackageManager().getPackageInfo("com.twitter.android", 0);

                    if (info.applicationInfo.enabled) {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=251097096"));
                    } else {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/lotehector"));
                    }
                    startActivity(intent);

                } catch (PackageManager.NameNotFoundException e) {
//                    Log.e("MAPA", e.getMessage());
                    System.out.println(e.getMessage());
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/lotehector"));
                    startActivity(intent);

                }
            }
        });

        return root;
    }
}