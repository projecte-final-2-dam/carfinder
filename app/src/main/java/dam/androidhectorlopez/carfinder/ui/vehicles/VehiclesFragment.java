package dam.androidhectorlopez.carfinder.ui.vehicles;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import dam.androidhectorlopez.carfinder.R;
import dam.androidhectorlopez.carfinder.db.DBManager_cotxes;

import static androidx.core.content.PermissionChecker.checkSelfPermission;

public class VehiclesFragment extends Fragment {
    private RecyclerView rvCarList;
    private DBManager_cotxes DBManager;
    private MyAdapter myAdapter;
    private ImageView ivEmpty;
    private TextView tvEmpty;
    View root;
    private static final int REQUEST_ENABLE_BT = 1;
    LocationManager locationManager;
    BluetoothAdapter mBluetoothAdapter;
    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS
    };
    private static final int LOCATION_REQUEST = 1337 + 3;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_vehicle, container, false);
        DBManager = new DBManager_cotxes(getContext());
        myAdapter = new MyAdapter(DBManager);
        ivEmpty = root.findViewById(R.id.ivEmpty);
        tvEmpty = root.findViewById(R.id.tvEmpty);

        setUI();

        return root;
    }

    private void setUI() {
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        rvCarList = root.findViewById(R.id.rvCarsList);
        rvCarList.setHasFixedSize(true);
        rvCarList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        rvCarList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        myAdapter.getData();
        rvCarList.setAdapter(myAdapter);

        if (myAdapter.getItemCount() == 0) {
            ivEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.VISIBLE);
        } else {
            ivEmpty.setVisibility(View.INVISIBLE);
            tvEmpty.setVisibility(View.INVISIBLE);
        }
        checkLocation();
        checkBluetooth();
        if (!canAccessLocation()) {
            requestPermissions(INITIAL_PERMS, LOCATION_REQUEST);
        }
    }

    /**
     * Aci comprovem si el bluetooth està activat
     */
    private void checkBluetooth() {

        if (!mBluetoothAdapter.isEnabled()) {
            Intent turnOnIntent = new Intent
                    (BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);
        }

    }

    /**
     * Aci comprovem si la localitzacio esta activada
     *
     * @return
     */
    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    /**
     * Aci el que fem es mostrar l'alerta per a activar els serveis d'ubicació
     */
    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(R.string.dialog_Ubicacio)
                .setMessage(R.string.msg_Ubicacio)
                .setPositiveButton(R.string.btUbicacio, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton(R.string.btCancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        try {
                            finalize();
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @SuppressLint({"NewApi", "WrongConstant"})
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(getContext(), perm));
    }
}