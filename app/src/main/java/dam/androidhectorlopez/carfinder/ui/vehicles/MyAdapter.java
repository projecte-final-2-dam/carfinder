package dam.androidhectorlopez.carfinder.ui.vehicles;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidhectorlopez.carfinder.R;
import dam.androidhectorlopez.carfinder.bluetooth.BTConexion;
import dam.androidhectorlopez.carfinder.db.DBManager_cotxes;
import dam.androidhectorlopez.carfinder.models.Car;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> implements View.OnClickListener {
    private DBManager_cotxes DBManager;
    private ArrayList<Car> myCarList;
    private View.OnClickListener listener;


    // class for each item
    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvNick;
        TextView tvBrand;
        TextView tvModel;
        TextView tvPlate;
        TextView tvId;

        DBManager_cotxes DBManager;
        Car car;
        BTConexion btConexion;


        public MyViewHolder(final View view) {
            super(view);

            this.tvNick = view.findViewById(R.id.tvcNick);
            this.tvBrand = view.findViewById(R.id.tvcBrand);
            this.tvModel = view.findViewById(R.id.tvcModel);
            this.tvPlate = view.findViewById(R.id.tvcPlate);
            this.tvId = view.findViewById(R.id.tvcId);

            DBManager = new DBManager_cotxes(itemView.getContext());


            // Aci es on connectem el dispositiu android al bluetooth del vehicle,
            // sempre i cuan el vehicle dispose de bluetooth
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.i("MAPA", "presionat en " + getAdapterPosition());
                    car = DBManager.getCar(Integer.parseInt((String) tvId.getText()));
                    if (car.isBluetooth()) {
                        Toast.makeText(itemView.getContext(), "Presionat en " + tvId.getText() + " " + tvNick.getText() + " TE bluetooth", Toast.LENGTH_LONG).show();

                        btConexion = new BTConexion(Integer.parseInt(tvId.getText().toString()), itemView.getContext());
                        btConexion.Connect();

                    } else {
                        Toast.makeText(itemView.getContext(), "Presionat en " + tvId.getText() + " " + tvNick.getText() + " NO bluetooth", Toast.LENGTH_LONG).show();

                    }


                }

            });
        }


        // sets viewHolder views with data
        public void bind(Car car) {
//            this.tvId.setText(String.valueOf(task.get_id()));
            this.tvNick.setText(car.getNick());
            this.tvBrand.setText(car.getBrand());
            this.tvModel.setText(car.getModel());
            this.tvPlate.setText(car.getLicense());
            this.tvId.setText(car.getId() + "");
        }
    }

    // constructor: DBManager_cotxes gets DB data
    public MyAdapter(DBManager_cotxes DBManager) {
        this.DBManager = DBManager;
    }

    // get data from DB
    public void getData() {
        this.myCarList = DBManager.getCars();
        notifyDataSetChanged();
    }


    // creates new view item: Layout Manager calls this method
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create item view:
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.caritem, parent, false);
        itemLayout.setOnClickListener(this);
        return new MyViewHolder(itemLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        // bind viewHoolder with data at: position
        viewHolder.bind(myCarList.get(position));
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);

        }
    }


    @Override
    public int getItemCount() {
        return myCarList.size();
    }


}
