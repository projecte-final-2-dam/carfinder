package dam.androidhectorlopez.carfinder.bluetooth;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import dam.androidhectorlopez.carfinder.R;
import dam.androidhectorlopez.carfinder.db.DBManager_bluetooth;
import dam.androidhectorlopez.carfinder.db.DBManager_ubicacions;
import dam.androidhectorlopez.carfinder.models.DevBluetooth;

import static androidx.core.content.ContextCompat.getSystemService;

public class BTConexion {

    //Handler es un control para mensajes
    Handler bluetoothIn;

    //Estado del manejador
    final int handlerState = 0;
    int id;
    Context context;
    DBManager_bluetooth dbManager_bluetooth;
    String macAddress;
    DevBluetooth devBluetooth;
    BluetoothAdapter bluetoothAdapter;
    BluetoothDevice device;
    BluetoothSocket socket;
    ConexionThread MyConexionBT;
    boolean estado = false;
    DBManager_ubicacions dbManager_ubicacions;

    public BTConexion(int id, Context context) {
        this.id = id;
        this.context = context;
    }

    public void Connect() {
        dbManager_bluetooth = new DBManager_bluetooth(context);
        devBluetooth = dbManager_bluetooth.getDev(id);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        /**
         * Mirar aci si canvia el boolea
         */
        device = bluetoothAdapter.getRemoteDevice(devBluetooth.getMacAddress());
        try {
            //Crea el socket sino esta conectado
            if (!estado) {
                Log.i("MAPA", "Abans de crear el dispositiu " + device.getAddress() + " " + device.getName());
                socket = BluetoothManager.connect(device);
                estado = socket.isConnected();
                Log.i("MAPA", "Despres de comprovar " + socket.isConnected());

            }

        } catch (IOException e) {
            Toast.makeText(context, "La creacción del Socket fallo", Toast.LENGTH_LONG).show();
            Log.e("MAPA", "Creacio " + e.getMessage());

        }

        /**
         * Mirar aci ja que aci dona castanyada si es connecta
         * amb un dispositiu bluetooth diferent al BT Receiver
         */
        // Establece la conexión con el socket Bluetooth.
        try {
            //Realiza la conexion si no se a hecho
            if (estado) {

                Thread.sleep(1000);
                Log.i("MAPA", "Abans del thread " + estado);
                MyConexionBT = new ConexionThread(socket);
                Log.i("MAPA", "Abans d'iniciar el thread");


                MyConexionBT.start();
                Toast.makeText(context, "Conexion Realizada Exitosamente", Toast.LENGTH_SHORT).show();
            } else {
//                Toast.makeText(context, "Ya esta vinculado", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            try {
                Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("MAPA", "Error" + e.toString());
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                socket.close();
            } catch (IOException e2) {
                Log.e("MAPA", "Error tancant el socket" + e2.getMessage());
            }
        }

    }


    private class ConexionThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConexionThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e("MAPA", "THREAD ERROR " + e.getLocalizedMessage());
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[256];
            int bytes;
            Log.i("MAPA", "Fin hola ");


            while (true) {
                // Se mantiene en modo escucha para determinar el ingreso de datos
                try {
                    Log.i("MAPA", "MISS\t");

                    bytes = mmInStream.read(buffer);
                    Log.i("MAPA", "MISS\t2");

                    String readMessage = new String(buffer, 0, bytes);
                    Log.i("MAPA", "MISS\t3");

                    // Envia los datos obtenidos hacia el evento via handler

//                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                    Log.i("MAPA", "MISS\t4");

                } catch (IOException e) {
                    Log.e("MAPA", "Fin " + e.getMessage());
                    try {
                        socket.close();
                    } catch (IOException ex) {
                        Log.e("MAPA", "Error dins del thread" + ex.getMessage());
                    }

                    break;
                }
            }
            addLocation();
        }


    }

    private void addLocation() {
        dbManager_ubicacions = new DBManager_ubicacions(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationManager locationManager;
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            dbManager_ubicacions.insert(id, location.getLongitude(), location.getLatitude());
            Log.i("MAPA", "LOCATION ADDED");


            Handler mHandler = new Handler(Looper.getMainLooper()) {
                public void handleMessage(Message msg) {
                    Toast.makeText(context, R.string.dialog_ubic, Toast.LENGTH_LONG).show();
                    Log.i("MAPA", "dins del handleMessage");

                }
            };
            Message message = mHandler.obtainMessage();
            message.sendToTarget();

            estado = false;
            String msg = context.getResources().getString(R.string.dialog_ubic);
            Log.i("MAPA", msg);
            notify(msg);
        }
    }

    /**
     * Creacio de notificacions
     *
     * @param eventName
     */
    private void notify(String eventName) {
        String activityName = this.getClass().getSimpleName();
        String CHANNEL_ID = "CarFinder";

        //Aci comprovem que la versio d'android es major o igual a Android 8.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Aci creem el canal de la notificacio, aixi com li donem la prioritat
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "CarFinder", NotificationManager.IMPORTANCE_DEFAULT);

            //Aci li donem el que mostrara la notificacio
            notificationChannel.setDescription("Lifecycle events");
            notificationChannel.setShowBadge(true);

            //Es una classe que ens ajuda a crear les notificacions d'una forma mes senzilla
            NotificationManager notificationManager = getSystemService(context, NotificationManager.class);

            //Aci comprovem que la notificacio no esta buida
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }


            NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID).setContentTitle(eventName).setAutoCancel(true).setSmallIcon(R.mipmap.ic_launcher);

            //Aci passem l'identificador de la notificacio i la creem
            notificationManagerCompat.notify((int) System.currentTimeMillis(), notificationBuilder.build());
        }
    }

}
