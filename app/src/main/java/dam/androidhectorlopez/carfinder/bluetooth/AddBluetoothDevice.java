package dam.androidhectorlopez.carfinder.bluetooth;

import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Set;

import dam.androidhectorlopez.carfinder.R;
import dam.androidhectorlopez.carfinder.db.DBManager_bluetooth;
import dam.androidhectorlopez.carfinder.db.DBManager_cotxes;

public class AddBluetoothDevice extends Dialog {

    private ListView lvBluetooth;
    private Button btBluetooth;
    private ArrayList<String> bluetoothDevices;
    private BluetoothAdapter mBluetoothAdapter;
    private ArrayAdapter<String> bAdapter;
    private DBManager_bluetooth dbManager_bluetooth;
    private DBManager_cotxes dbManager_cotxes;
    private String name;
    private String macAddress;


    public AddBluetoothDevice(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bluetooth_device);
        dbManager_bluetooth = new DBManager_bluetooth(getContext());
        dbManager_cotxes = new DBManager_cotxes(getContext());
        setUI();
    }


    private void setUI() {
        lvBluetooth = findViewById(R.id.lv_Bluetooth);
        btBluetooth = findViewById(R.id.btBlu);
        bluetoothDevices = new ArrayList<>();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        final Set<BluetoothDevice> pairedDevices;


        pairedDevices = mBluetoothAdapter.getBondedDevices();

        for (BluetoothDevice device : pairedDevices) {
            bluetoothDevices.add(device.getName() + "\t" + device.getAddress());
        }
        bAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_single_choice, bluetoothDevices);
        lvBluetooth.setAdapter(bAdapter);

        lvBluetooth.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvBluetooth.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                parent.setSelection(position);
                String bt = (String) parent.getItemAtPosition(position);
                String[] parts = bt.split("\t");
                name = parts[0];
                macAddress = parts[1];

                Toast.makeText(getContext(), "Selected " + position + " bt " + bt, Toast.LENGTH_LONG).show();
            }
        });


        btBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int id = dbManager_cotxes.lastCarId();
                    dbManager_bluetooth.insert(id, name, macAddress);
                    dismiss();
                    showAlert();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });


    }

    private void showAlert() {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(R.string.menu_afegir).setMessage(R.string.dialog_car).setPositiveButton(R.string.btAccept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                try {
                    finalize();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });

        dialog.show();
    }


}