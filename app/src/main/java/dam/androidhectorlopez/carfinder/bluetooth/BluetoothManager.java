package dam.androidhectorlopez.carfinder.bluetooth;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.UUID;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class BluetoothManager {
    private static final String NAME_SECURE = "PhoneGapBluetoothSerialServiceSecure";
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    /**
     * Instancia el Socket Bluetooth del dispositiu remot i el connecta
     * <p/>
     *
     * @param dev El dispositiu a connectar
     * @return El Socket Bluetooth
     * @throws IOException
     */
    public static BluetoothSocket connect(BluetoothDevice dev) throws IOException {
        BluetoothSocket sock = null;
        BluetoothSocket sockFallback = null;

        Log.d("MAPA", "Comencant connexio bluetooth");
        try {
            sock = dev.createRfcommSocketToServiceRecord(MY_UUID);
            sock.connect();
        } catch (Exception e1) {
            Log.e("MAPA", "Error establint connexio, Tornant enrere", e1);
            Class<?> clazz = sock.getRemoteDevice().getClass();
            Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
            try {
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};
                sockFallback = (BluetoothSocket) m.invoke(sock.getRemoteDevice(), params);
                sockFallback.connect();
                sock = sockFallback;
                Log.i("MAPA", "BLUETOOTH MANAGER " + sock.getRemoteDevice().getName());
            } catch (Exception e2) {

                Log.e("MAPA", "No s'ha pogut tornar enrere mentre s'establia la connexio bluetoth", e2);
                throw new IOException(e2.getMessage());
            }
        }
        return sock;
    }
}