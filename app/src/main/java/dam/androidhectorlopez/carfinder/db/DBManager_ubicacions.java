package dam.androidhectorlopez.carfinder.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;

import dam.androidhectorlopez.carfinder.models.Ubicacio;


public class DBManager_ubicacions {
    private DBHelper todoListDBHelper;

    public DBManager_ubicacions(Context context) {
        todoListDBHelper = DBHelper.getInstance(context);
    }


    // CREATE new ros
    public void insert(int carId, Double lon, Double lat) {
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(DBContract.Ubicacions.ID_COTXE, carId);
            contentValue.put(DBContract.Ubicacions.LONGITUDE, lon);
            contentValue.put(DBContract.Ubicacions.LATITUDE, lat);


            sqLiteDatabase.insert(DBContract.Ubicacions.TABLE_NAME, null, contentValue);

            checkCars(carId);

            // getWritableDatabase & getReadableDatabase() are expensive, you should leave your database connection
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }

    /**
     * En aquest metode el que fem es comprovar quantes ubicacions d'un mateix vehicle tenim,
     * si en tenim més de 2 es borrara la ubicacio mes antiga
     *
     * @param carId
     */
    private void checkCars(int carId) {
        ArrayList<Ubicacio> ubicacions = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        String ubis = "SELECT *"
                + " FROM " + DBContract.Ubicacions.TABLE_NAME +
                " WHERE " + DBContract.Ubicacions.ID_COTXE + " = " + carId;

        Cursor cursorTodoList = sqLiteDatabase.rawQuery(ubis, null);

        if (cursorTodoList != null) {
            // get the column indexes for required columns
            int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions._ID);
            int carIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.ID_COTXE);
            int lonIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.LONGITUDE);
            int latIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.LATITUDE);


            //read data and add to ArrayList
            while (cursorTodoList.moveToNext()) {
                Ubicacio ubicacio = new Ubicacio(cursorTodoList.getInt(_idIndex),
                        cursorTodoList.getInt(carIndex),
                        cursorTodoList.getDouble(lonIndex),
                        cursorTodoList.getDouble(latIndex));
                ubicacions.add(ubicacio);

                Log.i("Ubi", "\t\t\t" + cursorTodoList.getInt(_idIndex));
            }


            while (ubicacions.size() > 2) {
                Log.i("Ubi", "\t\t\t" + ubicacions.size());
                delete(ubicacions.get(0).getId());
                ubicacions.remove(0);
            }
            // close cursor to free resources
            cursorTodoList.close();
        }
    }


    // Get all data from Cars table
    public ArrayList<Ubicacio> getUbicacions() {
        ArrayList<Ubicacio> ubicacionsList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{DBContract.Cars._ID,
                    DBContract.Ubicacions.ID_COTXE,
                    DBContract.Ubicacions.LONGITUDE,
                    DBContract.Ubicacions.LATITUDE};


            Cursor cursorTodoList = sqLiteDatabase.query(DBContract.Ubicacions.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions._ID);
                int carIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.ID_COTXE);
                int lonIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.LONGITUDE);
                int latIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.LATITUDE);


                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Ubicacio ubicacio = new Ubicacio(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getInt(carIndex),
                            cursorTodoList.getDouble(lonIndex),
                            cursorTodoList.getDouble(latIndex));
                    ubicacionsList.add(ubicacio);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        System.out.println("\t\t\t\t\tTamany");
        return ubicacionsList;
    }


    public ArrayList<Ubicacio> getUbs() {
        ArrayList<Ubicacio> ubicacions = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        String ubis = "SELECT " + DBContract.Ubicacions._ID + ", " + DBContract.Ubicacions.ID_COTXE + ", " + DBContract.Ubicacions.LONGITUDE
                + ", " + DBContract.Ubicacions.LATITUDE + ", " + DBContract.Cars.NICK
                + " FROM " + DBContract.Ubicacions.TABLE_NAME + " INNER JOIN  " + DBContract.Cars.TABLE_NAME
                + " ON " + DBContract.Ubicacions.TABLE_NAME + "." + DBContract.Ubicacions.ID_COTXE + " = " + DBContract.Cars.TABLE_NAME + "." + DBContract.Cars._ID;

        Cursor cursorTodoList = sqLiteDatabase.rawQuery(ubis, null);

        if (cursorTodoList != null) {
            // get the column indexes for required columns
            int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions._ID);
            int carIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.ID_COTXE);
            int lonIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.LONGITUDE);
            int latIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.LATITUDE);
            int nameIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.NICK);


            //read data and add to ArrayList
            while (cursorTodoList.moveToNext()) {
                Ubicacio ubicacio = new Ubicacio(cursorTodoList.getInt(_idIndex),
                        cursorTodoList.getInt(carIndex),
                        cursorTodoList.getDouble(lonIndex),
                        cursorTodoList.getDouble(latIndex),
                        cursorTodoList.getString(nameIndex));
                ubicacions.add(ubicacio);
                Log.i("MAPA", cursorTodoList.getString(nameIndex));
            }
            // close cursor to free resources
            cursorTodoList.close();
        }
        return ubicacions;
    }

    public ArrayList<Ubicacio> getLastsUbs() {
        ArrayList<Ubicacio> ubicacions = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        String ubis = "SELECT " + DBContract.Ubicacions._ID + ", " + DBContract.Ubicacions.ID_COTXE + ", " + DBContract.Ubicacions.LONGITUDE
                + ", " + DBContract.Ubicacions.LATITUDE + ", " + DBContract.Cars.NICK
                + " FROM " + DBContract.Ubicacions.TABLE_NAME + " INNER JOIN  " + DBContract.Cars.TABLE_NAME
                + " ON " + DBContract.Ubicacions.TABLE_NAME + "." + DBContract.Ubicacions.ID_COTXE + " = " + DBContract.Cars.TABLE_NAME + "." + DBContract.Cars._ID
                + " GROUP BY " + DBContract.Ubicacions.ID_COTXE;

        Cursor cursorTodoList = sqLiteDatabase.rawQuery(ubis, null);

        if (cursorTodoList != null) {
            // get the column indexes for required columns
            int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions._ID);
            int carIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.ID_COTXE);
            int lonIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.LONGITUDE);
            int latIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Ubicacions.LATITUDE);
            int nameIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.NICK);


            //read data and add to ArrayList
            while (cursorTodoList.moveToNext()) {
                Ubicacio ubicacio = new Ubicacio(cursorTodoList.getInt(_idIndex),
                        cursorTodoList.getInt(carIndex),
                        cursorTodoList.getDouble(lonIndex),
                        cursorTodoList.getDouble(latIndex),
                        cursorTodoList.getString(nameIndex));
                ubicacions.add(ubicacio);
                Log.i("MAPA", cursorTodoList.getString(nameIndex));
            }
            // close cursor to free resources
            cursorTodoList.close();
        }
        return ubicacions;
    }


    public void close() {
        todoListDBHelper.close();       // closes any opened database object
    }

    /**
     * Aci actualitzem el contingut de cada element al obrir el EditCarActivity
     */
    public void update(int id, int carId, Double lon, Double lat) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(DBContract.Ubicacions.ID_COTXE, carId);
            contentValue.put(DBContract.Ubicacions.LONGITUDE, lon);
            contentValue.put(DBContract.Ubicacions.LATITUDE, lat);

            sqLiteDatabase.update(DBContract.Ubicacions.TABLE_NAME, contentValue, "_idU=" + id, null);


        }
    }

    public void delete(int id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(DBContract.Ubicacions.TABLE_NAME, "_idU=" + id, null);

        }

    }


    public void deleteAll() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(DBContract.Ubicacions.TABLE_NAME, null, null);

        }

    }


}
