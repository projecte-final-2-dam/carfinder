package dam.androidhectorlopez.carfinder.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import dam.androidhectorlopez.carfinder.models.Car;


public class DBManager_cotxes {
    private DBHelper todoListDBHelper;

    public DBManager_cotxes(Context context) {
        todoListDBHelper = DBHelper.getInstance(context);
    }


    // CREATE new ros
    public void insert(String nick, String brand, String model, String license, Boolean bluetooth) {
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(DBContract.Cars.NICK, nick);
            contentValue.put(DBContract.Cars.BRAND, brand);
            contentValue.put(DBContract.Cars.MODEL, model);
            contentValue.put(DBContract.Cars.LICENSE, license);
            contentValue.put(DBContract.Cars.BLUETOOTH, bluetooth);


            sqLiteDatabase.insert(DBContract.Cars.TABLE_NAME, null, contentValue);

            // getWritableDatabase & getReadableDatabase() are expensive, you should leave your database connection
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }


    // Get all data from Cars table
    public ArrayList<Car> getCars() {
        ArrayList<Car> carsList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{DBContract.Cars._ID,
                    DBContract.Cars.NICK,
                    DBContract.Cars.BRAND,
                    DBContract.Cars.MODEL,
                    DBContract.Cars.LICENSE,
                    DBContract.Cars.BLUETOOTH};


            Cursor cursorTodoList = sqLiteDatabase.query(DBContract.Cars.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars._ID);
                int nickIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.NICK);
                int brandIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.BRAND);
                int modelIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.MODEL);
                int licenseIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.LICENSE);
                int bluetoothIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.BLUETOOTH);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Car car = new Car(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(nickIndex),
                            cursorTodoList.getString(brandIndex),
                            cursorTodoList.getString(modelIndex),
                            cursorTodoList.getString(licenseIndex),
                            (cursorTodoList.getInt(bluetoothIndex) == 1));
                    carsList.add(car);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return carsList;
    }


    public Car getCar(int id) {
        Car car = null;
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{DBContract.Cars._ID,
                    DBContract.Cars.NICK,
                    DBContract.Cars.BRAND,
                    DBContract.Cars.MODEL,
                    DBContract.Cars.LICENSE,
                    DBContract.Cars.BLUETOOTH};


            Cursor cursorTodoList = sqLiteDatabase.query(DBContract.Cars.TABLE_NAME,
                    projection,                     // The columns toView return
                    "_id = " + id,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars._ID);
                int nickIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.NICK);
                int brandIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.BRAND);
                int modelIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.MODEL);
                int licenseIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.LICENSE);
                int bluetoothIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.BLUETOOTH);

                //read data and add to ArrayList
                cursorTodoList.moveToFirst();
                car = new Car(cursorTodoList.getInt(_idIndex),
                        cursorTodoList.getString(nickIndex),
                        cursorTodoList.getString(brandIndex),
                        cursorTodoList.getString(modelIndex),
                        cursorTodoList.getString(licenseIndex),
                        (cursorTodoList.getInt(bluetoothIndex) == 1));


                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return car;
    }

    public ArrayList<String> getUbCar() {
        ArrayList<String> carsList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{DBContract.Cars._ID,
                    DBContract.Cars.NICK,
                    DBContract.Cars.BRAND,
                    DBContract.Cars.MODEL,
                    DBContract.Cars.LICENSE,
                    DBContract.Cars.BLUETOOTH};


            Cursor cursorTodoList = sqLiteDatabase.query(DBContract.Cars.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars._ID);
                int nickIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.NICK);
                int brandIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.BRAND);
                int modelIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.MODEL);
                int licenseIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.LICENSE);
                int bluetoothIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.BLUETOOTH);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Car car = new Car(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(nickIndex),
                            cursorTodoList.getString(brandIndex),
                            cursorTodoList.getString(modelIndex),
                            cursorTodoList.getString(licenseIndex),
                            (cursorTodoList.getInt(bluetoothIndex) == 1));
                    carsList.add(cursorTodoList.getInt(_idIndex) + "\t" + cursorTodoList.getString(nickIndex));
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return carsList;
    }

    public int lastCarId() {
        int id = -1;

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();
        String sel = "SELECT " + DBContract.Cars._ID + " FROM " + DBContract.Cars.TABLE_NAME + " ORDER BY " + DBContract.Cars._ID + " DESC LIMIT 1";

        Cursor cursorTodoList = sqLiteDatabase.rawQuery(sel, null);
        if (cursorTodoList != null) {
            int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars._ID);
            cursorTodoList.moveToFirst();
            Log.i("MAPA", id + "");
            id = cursorTodoList.getInt(_idIndex);
            Log.i("MAPA", id + "");
        }


        return id;

    }

    public void close() {
        todoListDBHelper.close();       // closes any opened database object
    }

    /**
     * Aci actualitzem el contingut de cada element al obrir el EditCarActivity
     */
    public void update(int id, String nick, String brand, String model, String license, Boolean bluetooth) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(DBContract.Cars.NICK, nick);
            contentValue.put(DBContract.Cars.BRAND, brand);
            contentValue.put(DBContract.Cars.MODEL, model);
            contentValue.put(DBContract.Cars.LICENSE, license);
            contentValue.put(DBContract.Cars.BLUETOOTH, bluetooth);

            sqLiteDatabase.update(DBContract.Cars.TABLE_NAME, contentValue, "_id=" + id, null);


        }
    }

    public void delete(int id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(DBContract.Cars.TABLE_NAME, "_id=" + id, null);

        }

    }

    public void deleteCompl() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        String stat = "Completed";
        if (sqLiteDatabase != null) {
//            sqLiteDatabase.delete(DBContract.Cars.TABLE_NAME, DBContract.Cars.STATUS + "=" + stat, null);
            sqLiteDatabase.execSQL("DELETE FROM " + DBContract.Cars.TABLE_NAME + " WHERE status='Completed'");
        }
    }

    public void deleteAll() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(DBContract.Cars.TABLE_NAME, null, null);

        }

    }


//    public ArrayList<Car> getSelection(String stat) {
//        ArrayList<Car> CarList = new ArrayList<>();
//        String[] tipus = new String[]{stat};
//        System.out.println("\t\t\t---------------->" + stat);
//
//        // open database to read
//        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();
//
//        if (sqLiteDatabase != null) {
//            String[] projection = new String[]{DBContract.Cars._ID,
//                    DBContract.Cars.TODO,
//                    DBContract.Cars.TO_ACCOMPLISH,
//                    DBContract.Cars.DESCRIPTION,
//                    DBContract.Cars.PRIORITY,
//                    DBContract.Cars.STATUS};
////
////            CarList.clear();
//            System.out.println(CarList.size());
////
//            Cursor cursorTodoList = sqLiteDatabase.query(DBContract.Cars.TABLE_NAME,
//                    projection,                     // The columns toView return
//                    "status=?",                  // no WHERE clause
//                    tipus,               // no values for the WHERE clause
//                    null,                   // don't group hte rows
//                    null,                    // don't filter by row groups
//                    null);                  // don't sort rows
//
//
//            if (cursorTodoList != null) {
//                // get the column indexes for required columns
//                int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars._ID);
//                int todoIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.TODO);
//                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.TO_ACCOMPLISH);
//                int descriptionIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.DESCRIPTION);
//                int priorityIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.PRIORITY);
//                int statusIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.Cars.STATUS);
//
//                //read data and add to ArrayList
//                while (cursorTodoList.moveToNext()) {
//                    Car Car = new Car(cursorTodoList.getInt(_idIndex),
//                            cursorTodoList.getString(todoIndex),
//                            cursorTodoList.getString(to_AccomplishIndex),
//                            cursorTodoList.getString(descriptionIndex),
//                            cursorTodoList.getString(priorityIndex),
//                            cursorTodoList.getString(statusIndex));
//                    CarList.add(Car);
//                    System.out.println("\t\t" + cursorTodoList.getString(todoIndex));
//                }
//                System.out.println("\t\t\t\t\t\t--------->" + CarList.size());
//                // close cursor to free resources
//                cursorTodoList.close();
//            }
//        }
//        return CarList;
//    }


}
