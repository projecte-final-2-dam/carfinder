package dam.androidhectorlopez.carfinder.db;

/**
 * @author Hector Lopez
 * Class that defines TodoListDB schema
 */

public final class DBContract {
    // Common fields to all DB

    // Database name
    public static final String DB_NAME = "cars.DB";
    // Database Version
    public static final int DB_VERSION = 1;

    // To prevent someone from accidentally instantiating the contract class:
    // make the constructor private.
    private DBContract() {

    }

    // schema

    // TABLE TASKS: Inner class that defines the table Tasks contents
    public static class Cars {
        // Table name
        public static final String TABLE_NAME = "CARS";

        // Columns names
        public static final String _ID = "_id";
        public static final String NICK = "nick";
        public static final String BRAND = "brand";
        public static final String MODEL = "model";
        public static final String LICENSE = "license";
        public static final String BLUETOOTH = "bluetooth";

        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Cars.TABLE_NAME
                + " ("
                + Cars._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Cars.NICK + " TEXT NOT NULL, "
                + Cars.BRAND + " TEXT, "
                + Cars.MODEL + " TEXT, "
                + Cars.LICENSE + " TEXT, "
                + Cars.BLUETOOTH + " BOOLEAN "
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Cars.TABLE_NAME;
    }

    // TABLE TASKS: Inner class that defines the table Tasks contents
    public static class Ubicacions {
        // Table name
        public static final String TABLE_NAME = "UBICATIONS";

        // Columns names
        public static final String _ID = "_idU";
        public static final String ID_COTXE = "id_cotxe";
        public static final String LONGITUDE = "lon";
        public static final String LATITUDE = "lat";


        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Ubicacions.TABLE_NAME
                + " ("
                + Ubicacions._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Ubicacions.ID_COTXE + " INTEGER NOT NULL, "
                + Ubicacions.LONGITUDE + " DOUBLE, "
                + Ubicacions.LATITUDE + " DOUBLE, "
                + "FOREIGN KEY(" + Ubicacions.ID_COTXE + ") references " + Cars.TABLE_NAME + "(" + Cars._ID + ") "
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Ubicacions.TABLE_NAME;
    }

    public static class DispBt {
        // Table name
        public static final String TABLE_NAME = "DISPBT";

        // Columns names
        public static final String _ID = "_idB";
        public static final String ID_COTXE = "id_cotxe";
        public static final String NOM = "name";
        public static final String MAC = "mac";


        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + DispBt.TABLE_NAME
                + " ("
                + DispBt._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DispBt.ID_COTXE + " INTEGER NOT NULL, "
                + DispBt.NOM + " TEXT, "
                + DispBt.MAC + " TEXT, "
                + "FOREIGN KEY(" + DispBt.ID_COTXE + ") references " + Cars.TABLE_NAME + "(" + Cars._ID + ") "
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + DispBt.TABLE_NAME;
    }
}
