package dam.androidhectorlopez.carfinder.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import dam.androidhectorlopez.carfinder.models.DevBluetooth;


public class DBManager_bluetooth {
    private DBHelper todoListDBHelper;

    public DBManager_bluetooth(Context context) {
        todoListDBHelper = DBHelper.getInstance(context);
    }


    // CREATE new ros
    public void insert(int carId, String name, String mac) {
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(DBContract.DispBt.ID_COTXE, carId);
            contentValue.put(DBContract.DispBt.NOM, name);
            contentValue.put(DBContract.DispBt.MAC, mac);


            sqLiteDatabase.insert(DBContract.DispBt.TABLE_NAME, null, contentValue);

            // getWritableDatabase & getReadableDatabase() are expensive, you should leave your database connection
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }


    // Get all data from DispBt table
    public ArrayList<DevBluetooth> getDispBt() {
        ArrayList<DevBluetooth> DispBtList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{DBContract.Cars._ID,
                    DBContract.DispBt.ID_COTXE,
                    DBContract.DispBt.NOM,
                    DBContract.DispBt.MAC};


            Cursor cursorTodoList = sqLiteDatabase.query(DBContract.DispBt.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.DispBt._ID);
                int carIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.DispBt.ID_COTXE);
                int nameIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.DispBt.NOM);
                int macIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.DispBt.MAC);


                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    DevBluetooth devBluetooth = new DevBluetooth(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getInt(carIndex),
                            cursorTodoList.getString(nameIndex),
                            cursorTodoList.getString(macIndex));
                    DispBtList.add(devBluetooth);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return DispBtList;
    }


    public DevBluetooth getDev(int idCar) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();
        DevBluetooth devBluetooth = null;

        String dev = "SELECT *"
                + " FROM " + DBContract.DispBt.TABLE_NAME
                + " WHERE " + idCar + " = " + DBContract.DispBt.ID_COTXE;

        Cursor cursorTodoList = sqLiteDatabase.rawQuery(dev, null);

        if (cursorTodoList != null) {
            // get the column indexes for required columns
            int _idIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.DispBt._ID);
            int carIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.DispBt.ID_COTXE);
            int nameIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.DispBt.NOM);
            int macIndex = cursorTodoList.getColumnIndexOrThrow(DBContract.DispBt.MAC);

            cursorTodoList.moveToFirst();

            //read data and add to ArrayList

            devBluetooth = new DevBluetooth(cursorTodoList.getInt(_idIndex),
                    cursorTodoList.getInt(carIndex),
                    cursorTodoList.getString(nameIndex),
                    cursorTodoList.getString(macIndex));

            Log.i("MAPA", cursorTodoList.getString(nameIndex));

            // close cursor to free resources
            cursorTodoList.close();
        }
        return devBluetooth;
    }


    public void close() {
        todoListDBHelper.close();       // closes any opened database object
    }

    /**
     * Aci actualitzem el contingut de cada element al obrir el EditCarActivity
     */
    public void update(int id, int carId, String name, String mac) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(DBContract.DispBt.ID_COTXE, carId);
            contentValue.put(DBContract.DispBt.NOM, name);
            contentValue.put(DBContract.DispBt.MAC, mac);

            sqLiteDatabase.update(DBContract.DispBt.TABLE_NAME, contentValue, "_id=" + id, null);


        }
    }

    public void delete(int id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(DBContract.DispBt.TABLE_NAME, "_id=" + id, null);
        }
    }


    public void deleteAll() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(DBContract.DispBt.TABLE_NAME, null, null);
        }
    }


}
