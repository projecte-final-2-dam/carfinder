package dam.androidhectorlopez.carfinder.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    // instance to SQLiteOpenHelper
    private static DBHelper instanceDBHelper;


    // This method assures only one instance of DBHelper for all the application.
    // Use the application context, to not leak Activity context

    public static synchronized DBHelper getInstance(Context context) {
        // instance must be unique
        if (instanceDBHelper == null) {
            instanceDBHelper = new DBHelper(context.getApplicationContext());
        }
        return instanceDBHelper;
    }

    // Constructor should be private to prevent direct instatiation
    private DBHelper(Context context) {
        super(context, DBContract.DB_NAME, null, DBContract.DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DBContract.Cars.CREATE_TABLE);
        sqLiteDatabase.execSQL(DBContract.Ubicacions.CREATE_TABLE);
        sqLiteDatabase.execSQL(DBContract.DispBt.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // the upgrade policy is simply discard the table and start over
        sqLiteDatabase.execSQL(DBContract.Cars.DELETE_TABLE);
        sqLiteDatabase.execSQL(DBContract.Ubicacions.DELETE_TABLE);
        sqLiteDatabase.execSQL(DBContract.DispBt.DELETE_TABLE);

        // create again the DB
        onCreate(sqLiteDatabase);
    }



    public void onDelete(SQLiteDatabase sqLiteDatabase){
        sqLiteDatabase.execSQL(DBContract.Cars.DELETE_TABLE);
        sqLiteDatabase.execSQL(DBContract.Ubicacions.DELETE_TABLE);
        sqLiteDatabase.execSQL(DBContract.DispBt.DELETE_TABLE);

    }
}
