package dam.androidhectorlopez.carfinder.models;

public class Car {
    private int id;
    private String nick;
    private String brand;
    private String model;
    private String license;
    private boolean bluetooth;


    public Car(int id, String nick, String brand, String model, String license, boolean bluetooth) {
        this.id = id;
        this.nick = nick;
        this.brand = brand;
        this.model = model;
        this.license = license;
        this.bluetooth = bluetooth;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public boolean isBluetooth() {
        return bluetooth;
    }

    public void setBluetooth(boolean bluetooth) {
        this.bluetooth = bluetooth;
    }
}
