package dam.androidhectorlopez.carfinder.models;

public class DevBluetooth {
    int id;
    int id_Car;
    String name;
    String macAddress;


    public DevBluetooth(int id, int id_Car, String name, String macAddress) {
        this.id = id;
        this.id_Car = id_Car;
        this.name = name;
        this.macAddress = macAddress;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_Car() {
        return id_Car;
    }

    public void setId_Car(int id_Car) {
        this.id_Car = id_Car;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}
