package dam.androidhectorlopez.carfinder.models;

public class Ubicacio {
    int id;
    int idCar;
    double lon;
    double lat;
    String nickCar;

    public Ubicacio(int id, int idCar, double lon, double lat, String nickCar) {
        this.id = id;
        this.idCar = idCar;
        this.lon = lon;
        this.lat = lat;
        this.nickCar = nickCar;
    }

    public Ubicacio(int id, int idCar, double lon, double lat) {
        this.id = id;
        this.idCar = idCar;
        this.lon = lon;
        this.lat = lat;
    }

    public Ubicacio(int id, double lon, double lat) {
        this.id = id;
        this.lon = lon;
        this.lat = lat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCar() {
        return idCar;
    }

    public void setIdCar(int idCar) {
        this.idCar = idCar;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getNickCar() {
        return nickCar;
    }

    public void setNickCar(String nickCar) {
        this.nickCar = nickCar;
    }
}
